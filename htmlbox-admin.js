$(document).ready(function() {
  var mouseXcord_old = 0;
  var mouseYcord_old = 0;
  var activespan = '';
  var whichspan = 0;
  var testA, testAthis, testAreaLeftX, testAreaTopY, testAreaRightX, testAreaBottomY;

  init();

  function startDrag(e) {
    testAreaLeftX = Drupal.absolutePosition(testAthis).x;
    testAreaTopY = Drupal.absolutePosition(testAthis).y;
    testAreaRightX = testAreaLeftX+testAthis.offsetWidth;
    testAreaBottomY = testAreaTopY+testAthis.offsetHeight;

    whichspan = 0;
    activespan = $(this);
    activespan.css('cursor', 'move');
    $('#htmlbox-buttons-manager span.'+ activespan.attr('class')).bind('mouseup', addToBox);
    activespan.unbind('mouseup', removeFromBox);
    mouseXcord_old = e.pageX;
    mouseYcord_old = e.pageY;
    $(document).mousemove(performDrag);

    if ($(this).parent().is('#htmlbox-droparea')) {
      $(document).bind('mouseup', endDrag2);
      activespan.bind('mouseup', removeFromBox).unbind('mouseup', addToBox);
    }
    else {
      $(document).bind('mouseup', endDrag);
    }
    return false;
  }
  
  function performDrag(e) {
    activespan.css({
        left: (e.pageX-mouseXcord_old) +'px',
        top: (e.pageY-mouseYcord_old) +'px'
      }).unbind('mouseup', addToBox).unbind('mouseup', removeFromBox);

    if (e.pageX < testAreaRightX && e.pageX > testAreaLeftX &&
      e.pageY < testAreaBottomY && e.pageY > testAreaTopY) {
      var distance;
      var leftOrRight;
      whichspan = 0;

      $('.helper', testA).remove();
      $('span', testA).each(function(){
        if (activespan.css('left') != $(this).css('left') ) {
          var marginTopBottom = (parseInt($(this).css('margin-left'), 10) +
            parseInt($(this).css('margin-right'), 10))/2;
          var spanLeftX = Drupal.absolutePosition(this).x;
          var spanTopY = Drupal.absolutePosition(this).y-marginTopBottom;
          var spanRightX = spanLeftX + this.offsetWidth;
          var spanBottomY = spanTopY + this.offsetHeight+(marginTopBottom * 2);
          var spanMiddlePoint = spanLeftX+this.offsetWidth/2;

          if (e.pageY < spanBottomY && e.pageY > spanTopY ) {
            if (distance > Math.abs(spanMiddlePoint-e.pageX) || distance == undefined) {
              leftOrRight = spanMiddlePoint - e.pageX;
              distance = Math.abs(leftOrRight);
              whichspan = $(this);
            }
          }
        }
      });

      var helper = '<span class="helper" style="height: '+
        (parseInt(activespan.css('height'), 10) - 2) +'px; padding-left: '+
        activespan.css('padding-left') +'; padding-right: '+
        activespan.css('padding-right') +';"></span>';

      if (whichspan) {
        if (leftOrRight<0) {
          whichspan.after(helper);
          whichspan.poz = 'after';
        }
        else {
          whichspan.before(helper);
        }
      }
      else testA.append(helper);
    }
    else {
      $('.helper', testA).remove();
    }

    return false;
  }

  function endDrag(e) {
    $(document).unbind('mousemove', performDrag).unbind('mouseup', endDrag);
    activespan.css('cursor', '');
    backanimate(e);
    updateOptions();
  }
  
  function endDrag2(e) {
    $(document).unbind('mousemove', performDrag).unbind('mouseup', endDrag2);
    activespan.css('cursor', '');
    backanimate2(e);
    updateOptions();
  }

  function addToBox() {
    $('.helper', testA).remove();
    if (!activespan.is('.htmlbox-separator_basic') && !activespan.is('.htmlbox-separator_dots')) activespan.css({cursor: ''}).hide();
    $(document).unbind('mousemove', performDrag).unbind('mouseup', endDrag);

    var clonespan = activespan.clone().css('display', 'block').bind('mousedown', startDrag);

    if (whichspan) {
      if (whichspan.poz == 'after') {
        whichspan.after(clonespan);
      }
      else {
        whichspan.before(clonespan);
      }
    }
    else {
      testA.append(clonespan);
    }
  }
  
  function removeFromBox() {
    activespan.remove();
    $(document).unbind('mousemove', performDrag).unbind('mouseup', endDrag2);
    $('#htmlbox-buttons-manager span.'+ activespan.attr('class')).show().bind('mousedown', startDrag);
  }
  
  function updateOptions() {
    var options = [];
    testA.find('span[@class]').each(function () {
      options.push($(this).attr('class').replace(/^htmlbox-/, ''));
    }).end();
    $('#edit-htmlbox-buttons-order').val(options.join(', '));
  }


  function backanimate(e) {
    activespan.css({left: '', top: ''});
    if (e.pageX < testAreaRightX && e.pageX > testAreaLeftX && e.pageY < testAreaBottomY && e.pageY > testAreaTopY ) {
      addToBox();
    }
    else {
      activespan.bind('mouseup', addToBox);
    }
  }

  function backanimate2(e) {
    $('.helper', testA).remove();

    if (e.pageX > testAreaRightX || e.pageX < testAreaLeftX || e.pageY > testAreaBottomY || e.pageY < testAreaTopY ) {
      activespan.remove();
      $('#htmlbox-buttons-manager span.'+ activespan.attr('class')).css('display', 'block').bind('mousedown', startDrag);
    }
    else {
      var clonespan = $('#htmlbox-buttons-manager span.'+ activespan.attr('class')).clone(false).css('display', 'block').unbind('mouseup', addToBox).bind('mousedown', startDrag);

      if (whichspan) {
        if (whichspan.poz == 'after') {
          whichspan.after(clonespan);
        }
        else {
          whichspan.before(clonespan);
        }
      }
      else {
        testA.append(clonespan);
      }
      activespan.remove();
    }
  }

  function init() {
    $('div.htmlbox-buttons').before('<div class="htmlbox-buttons htmlbox-buttons-js"><div id="htmlbox-droparea"></div><div id="htmlbox-buttons-manager"></div></div>').find('span').each(function () {
      $('#htmlbox-buttons-manager').append(this);
    }).end().remove();
    testA = $('#htmlbox-droparea');
    testAthis = testA[0];

    $.each($('#edit-htmlbox-buttons-order').val().split(','), function() {
      var elem = $('#htmlbox-buttons-manager').find('span.htmlbox-'+ $.trim(this));
      if (!elem.is('.htmlbox-separator_basic') && !elem.is('.htmlbox-separator_dots')) {
        elem.hide();
      }
      testA.append(elem.clone(false).show());
    });
    window.setTimeout("$('#edit-htmlbox-buttons-order').parents('div.form-item:first').children().not('label').not('.description').hide()", 10);

    var textNode = $('.htmlbox-buttons').find('span').bind('mousedown', startDrag).end().get(0).previousSibling;
    if (textNode.nodeType == 3) {
      textNode.nodeValue = Drupal.settings.htmlbox.order_description;
    }
  }
});