******************************************************************************

 HTMLBox WYSIWYG editor as textarea replacement.

 The module gives a wide range of configuration under
 Administer > Site configuration > HTMLBox settings
 (admin/settings/htmlbox).

******************************************************************************

 As an option to produce XHTML output download htmlparser.js [1],
 detailed in the article [2].

 [1] http://ejohn.org/files/htmlparser.js
 [2] http://ejohn.org/blog/pure-javascript-html-parser/
